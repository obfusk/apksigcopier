Source: apksigcopier
Section: misc
Priority: optional
Maintainer: FC Stegerman <flx@obfusk.net>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3-all,
               python3-click,
               python3-setuptools,
               pandoc,
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/obfusk/apksigcopier.git
Vcs-Browser: https://salsa.debian.org/obfusk/apksigcopier
Homepage: https://github.com/obfusk/apksigcopier
Rules-Requires-Root: no

Package: apksigcopier
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends},
Suggests: apksigner
Description: copy/extract/patch android apk signatures & compare apks
 apksigcopier is a tool for copying android APK signatures from a signed APK to
 an unsigned one (in order to verify reproducible builds).  It can also be used
 to compare two APKs with different signatures.  Its command-line tool offers
 four operations:
 .
  * copy signatures directly from a signed to an unsigned APK
  * extract signatures from a signed APK to a directory
  * patch previously extracted signatures onto an unsigned APK
  * compare two APKs with different signatures (requires apksigner)
